package main

import (
	"fmt"
	"pertama/calculation"
)

func main() {
	fmt.Println("Halo, belajar Golang")

	var name string = "Golang"
	var age int = 28
	age = 10
	fmt.Println(name)
	fmt.Println(age)

	// var nmax string
	// nmax = "Ruby on Rails"
	// fmt.Println(nmax)

	tambah := calculation.Add(9, 9)
	kali := calculation.Multiply(5, 10)

	fmt.Println(tambah)
	fmt.Println(kali)

	if age > 10 {
		fmt.Println("Boleh bermain game")
	} else if age == 10 {
		fmt.Println("Main gamenya jangan lama-lama")
	} else {
		fmt.Println("Kamu belum boleh bermain game")
	}

	score := 65
	var grade string

	if score <= 50 {
		grade = "E"
	} else if score <= 60 {
		grade = "D"
	} else if score <= 70 {
		grade = "C"
	} else {
		grade = "NULL"
	}

	fmt.Println(grade)

	number := 2

	switch number {
	case 1:
		fmt.Println("Satu")
	case 2:
		fmt.Println("Dua")
	case 3:
		fmt.Println("Tiga")
	default:
		fmt.Println("DEFAULT")
	}

	for i := 1; i <= 10; i++ {
		fmt.Println("Saya sedang belajar GO :", i)
	}

	i := 1
	for i <= 10 {
		fmt.Println("Saya sedang belajar GO :", i)
		i++
	}

	title := "GOlang the best language"

	for index, letter := range title {
		fmt.Println("index :", index, " letter :", string(letter))
	}

	for _, letter := range title {
		fmt.Println("letter :", string(letter))
	}

	for index, letter := range title {
		if index%2 == 0 {
			fmt.Println("index :", index, "letter :", string(letter))
		}
	}

	for index, letter := range title {
		letterSting := string(letter)

		// if letterSting == "a" || letterSting == "i" ||letterSting == "u" ||letterSting == "e" ||letterSting == "o" {
		// 	fmt.Println("index :", index, "letter :", string(letter))
		// }

		switch letterSting {
		case "a", "i", "u", "e", "o":
			fmt.Println("index :", index, "letter :", string(letter))
		}
	}

	var languages [5]string
	languages[0] = "Go"
	languages[1] = "Ruby"
	languages[2] = "JavaScript"
	languages[3] = "Java"
	languages[4] = "Python"

	language := [5]string{"Ruby", "Python", "Java", "GO", "C++"}

	bahasa := [...]string{
		"Inggris",
		"Korea",
		"Jepang",
		"Jerman",
		"Indonesia",
	}

	for index, lang := range languages {
		fmt.Println("Index : ", index, " language : ", lang)
	}

	fmt.Println(languages)
	length := len(languages)
	fmt.Println(length)
	fmt.Println(language)
	fmt.Println(bahasa)

	/* SLICE */

	var gamingConsoles []string
	gamingConsoles = append(gamingConsoles, "PlayStation4")
	gamingConsoles = append(gamingConsoles, "Nintendo Switch")
	gamingConsoles = append(gamingConsoles, "Xbox One")

	fmt.Println(gamingConsoles)

	for _, console := range gamingConsoles {
		fmt.Println(console)
	}

	/* MAP */

	var myMap map[string]int
	myMap = map[string]int{}

	myMap["ruby"] = 9
	myMap["JavaScript"] = 8
	myMap["go"] = 10

	fmt.Println(myMap["ruby"])

	myMaps := map[string]string{
		"ruby":       "is beautiful",
		"go":         "is super fast",
		"JavaScript": "hype",
	}
	fmt.Println(myMaps)

	for key, value := range myMap {
		fmt.Println("Key :", key, " Value :", value)
	}
	fmt.Println("===================================")

	delete(myMaps, "ruby")

	for key, value := range myMaps {
		fmt.Println("Key :", key, " Value :", value)
	}

	value, isAvailable := myMaps["python"]
	fmt.Println(isAvailable)
	fmt.Println(value)

	students := []map[string]string{
		{"name": "Adi", "score": "A"},
		{"name": "Rahmat", "score": "B"},
		{"name": "Wijaya", "score": "B"},
	}

	for _, student := range students {
		fmt.Println(student["name"], "-", student["score"])
	}

	/* COUNT MEDIAN */

	scores := [8]int{100, 80, 75, 92, 70, 93, 88, 67}
	var total int

	for _, score := range scores {
		total = total + score
	}

	panjang := len(scores)
	average := float64(total) / float64(panjang)

	fmt.Println(average)
	fmt.Println("===============================")

	var goodScores []int

	for _, score := range scores {
		if score >= 90 {
			goodScores = append(goodScores, score)
		}
	}
	fmt.Println(goodScores)

	/* FUNCTION */

	sentence := printMyResult("Saya sedang belajar Go")
	fmt.Println(sentence)
	result := add(10, 20)
	fmt.Println(result)
	luas, keliling := calculate(10, 2)
	fmt.Println(luas)
	fmt.Println(keliling)

	// sentence := TestAja()
	// fmt.Println(sentence)
}

func calculate(panjang, lebar int) (int, int) {
	luas := panjang * lebar
	keliling := 2 * (panjang + lebar)

	return luas, keliling
}

func add(number, numberTwo int) int {
	return number + numberTwo
}

func printMyResult(sentence string) string {
	newSentence := sentence + "Lang"
	return newSentence
}
